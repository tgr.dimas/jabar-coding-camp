//soal 1
// luas
const panjang = 6
const lebar = 4
let a = "luas = "
function multiply (panjang, lebar){
  return (panjang*lebar);
}
console.log(a.concat(multiply(panjang,lebar)));
// keliling
let b = "keliling = "
function add (panjang, lebar){
  return (2*(panjang+lebar));
}
console.log(b.concat(add(panjang,lebar)));

//soal 2
const newFunction = function literal(namaPertama, namaTerakhir){
    return {
      namaPertama,
      namaTerakhir,
      namaLengkap(){
        console.log(namaPertama + " " + namaTerakhir)
      }
    }
  }
   
  newFunction("William", "Imoh").namaLengkap() 

//soal 3
var biodata = {
    firstName: 'Muhammad',
    lastName: 'Iqbal Mubarok',
    address: 'Jalan Ranamnyar',
    hobby: 'playing football',
};
const {firstName, lastName, address, hobby} = biodata
console.log (firstName, lastName, address, hobby)

//soal 4
let west = ['Will', 'Chris', 'Sam', 'Holly']
let east = ['Gill', 'Brian', 'Noel', 'Maggie']
let combined = [...west, ...east]
console.log(combined)

//soal 5
const planet = 'earth' 
const view = 'glass' 
const before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`
console.log (before)
