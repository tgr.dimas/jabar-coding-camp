//soal 1
var nilai = 90

if (nilai>=85){
    console.log("A")
}
else if (nilai>=75&&nilai<85){
    console.log("B")
}
else if (nilai>=65&&nilai<75){
    console.log("C")
}
else if (nilai>55&&nilai<65){
    console.log("D")
}
else {
    console.log("E")
}

//soal 2
var tanggal = 6;
var bulan = 4;
var tahun = 1999
switch(bulan) {
    case 1: {
        console.log(tanggal+' '+'Januari'+' '+tahun); break;
    }
    case 2: {
        console.log(tanggal+' '+'Februari'+' '+tahun); break;
    }
    case 3: {
        console.log(tanggal+' '+'Maret'+' '+tahun); break;
    }
    case 4: {
        console.log(tanggal+' '+'April'+' '+tahun); break;
    }
    case 5: {
        console.log(tanggal+' '+'Mei'+' '+tahun); break;
    }
    case 6: {
        console.log(tanggal+' '+'Juni'+' '+tahun); break;
    }
    case 7: {
        console.log(tanggal+' '+'Juli'+' '+tahun); break;
    }
    case 8: {
        console.log(tanggal+' '+'Agustus'+' '+tahun); break;
    }
    case 9: {
        console.log(tanggal+' '+'September'+' '+tahun); break;
    }
    case 10: {
        console.log(tanggal+' '+'Oktober'+' '+tahun); break;
    }
    case 11: {
        console.log(tanggal+' '+'November'+' '+tahun); break;
    }
    case 12: {
        console.log(tanggal+' '+'Desember'+' '+tahun); break;
    }
    default:  {
        console.log(tanggal+' '+'(Bulan Lahir)'+' '+tahun); }
    }
  
//soal 3
var n = 3;
var a = "#"

for (var x = 0; x<n; x++){
    console.log(a);
    a=a.concat("#");
}

var n = 7;
var y = 0;
var b = "#"

while (y < n){
    console.log(b);
    b=b.concat("#");
    y++
}

//soal 4
// m=3
var m = 3;
m++;
var y = 1
var a = "==="

for (var x = 1; x < m; x++){
    if (y == 1){
        console.log (x + " - I love programming");
    }
    else if (y == 2){
        console.log (x + " - I love Javascript");
    }
    else if (y == 3){
        console.log (x + " - I love VueJS");
    }
y++;
if (x % 3 == 0){
    console.log(a);
    a = a.concat("===");
    y = 1;
}
}
// m=7
var m = 7;
m++;
var y = 1
var a = "==="

for (var x = 1; x < m; x++){
    if (y == 1){
        console.log (x + " - I love programming");
    }
    else if (y == 2){
        console.log (x + " - I love Javascript");
    }
    else if (y == 3){
        console.log (x + " - I love VueJS");
    }
y++;
if (x % 3 == 0){
    console.log(a);
    a = a.concat("===");
    y = 1;
}
}

// m=10
var m = 10;
m++;
var y = 1
var a = "==="

for (var x = 1; x < m; x++){
    if (y == 1){
        console.log (x + " - I love programming");
    }
    else if (y == 2){
        console.log (x + " - I love Javascript");
    }
    else if (y == 3){
        console.log (x + " - I love VueJS");
    }
y++;
if (x % 3 == 0){
    console.log(a);
    a = a.concat("===");
    y = 1;
}
}


