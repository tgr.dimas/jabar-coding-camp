// Soal 1

var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];

for (i = 0; i < 5; i++) {
  daftarHewan.sort();
  console.log(daftarHewan[i]);
}

// Soal 2

function introduce(data) {
  console.log("nama saya " + data.name + ", " + "umur saya " + data.age + " " + "tahun" + ", " + "alamat saya di " + data.address + ", " + "dan saya punya hobby yaitu " + data.hobby);
}

var data = { name: "Dimas Tegar", age: 22, address: "Cimahi", hobby: "Futsal" };

var perkenalan = introduce(data);
console.log(perkenalan);

// Soal 3

function hitung_huruf_vokal(kata) {
  const count = kata.match(/[aiueo]/gi).length;
  return count;
}

var hitung_1 = hitung_huruf_vokal("Dimas");

var hitung_2 = hitung_huruf_vokal("Tegar");

console.log(hitung_1, hitung_2);

// Soal 4

function hitung(angka) {
  return angka + angka - 2;
}
console.log(hitung(0));
console.log(hitung(1));
console.log(hitung(2));
console.log(hitung(3));
console.log(hitung(5));
