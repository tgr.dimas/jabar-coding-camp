// Soal 1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";
var kal1 = pertama.substr(0, 5) + pertama.substr(12, 7);
var kal2 = kedua.substr(0, 8) + kedua.substr(8, 10).toUpperCase();

console.log(kal1 + kal2); //...jawaban soal 1

//Soal 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

var satu = parseInt(kataPertama);
var dua = parseInt(kataKedua);
var tiga = parseInt(kataKetiga);
var empat = parseInt(kataKeempat);

console.log(satu + dua * tiga + empat); //...jawaban soal 2

//Soal 3
var kalimat = "wah javascript itu keren sekali";

var kataPertama = kalimat.substring(0, 3);
var kataKedua = kalimat.substring(4, 15); //....jawaban soal 3
var kataKetiga = kalimat.substring(15, 18);
var kataKeempat = kalimat.substring(19, 24);
var kataKelima = kalimat.substring(25); //....jawaban soal 3

console.log("Kata Pertama: " + kataPertama);
console.log("Kata Kedua: " + kataKedua);
console.log("Kata Ketiga: " + kataKetiga);
console.log("Kata Keempat: " + kataKeempat);
console.log("Kata Kelima: " + kataKelima);
